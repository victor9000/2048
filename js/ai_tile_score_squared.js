// the Ai.step function implements a greedy algorithm which
// attempts to maximize the sum of the squares of each title
// using a game tree for look aheads.  This means that the
// algorithm will consider a 4 tile to be more valueable than
// two 2 tiles, since 4^2 > 2^2 + 2^2.

function Ai() {
 
    this.init = function() {
        // This method is called when AI is first initialized.
    }
     
    this.restart = function() {
        // This method is called when the game is reset.
    }
     
    // this.step is called on every update.
    // Return one of these integers to move tiles on the grid:
    // 0: up, 1: right, 2: down, 3: left

    // Parameter grid contains current state of the game as Tile objects stored in grid.cells.
    // Top left corner is at grid.cells[0][0], top right: grid.cells[3][0], bottom left: grid.cells[0][3], bottom right: grid.cells[3][3].
    // Tile objects have .value property which contains the value of the tile. If top left corner has tile with 2, grid.cells[0][0].value == 2.
    // Array will contain null if there is no tile in the slot (e.g. grid.cells[0][3] == null if bottom left corner doesn't have a tile).

    // Grid has 2 useful helper methods:
    // .copy()    - creates a copy of the grid and returns it.
    // .move(dir) - can be used to determine what is the next state of the grid going to be if moved to that direction.
    //              This changes the state of the grid object, so you should probably copy() the grid before using this.
    //              Naturally the modified state doesn't contain information about new tiles.
    //              Method returns true if you can move to that direction, false otherwise.
    this.step = function(grid) {
        var id = '';
        var depth = 5;
        var score = getScore(grid);
        var gameTree = getNode(id, -1, grid, -1, -1, score);
        var positions = [];
        var move = -1;

        // create a game tree of each possible move,
        // and assign a score to each position
        loadGameTree(gameTree, depth, positions);

        // get the best move from the scored positions
        if (positions && positions.length > 0)
            move = getBestMove(positions);

        return move;
    }

    function loadGameTree(parentNode, maxHeight, positions) {

        var nodeLevel = parentNode.level + 1;

        // don't go further if we've reached the max height
        if (nodeLevel == maxHeight)
            return;

        // get moves        
        for (var i=0; i<4; i++) {
            var move = i;            
            var game = parentNode.game.copy();
            var canMove = game.move(move);

            // if we can't move then skip to next move
            if(!canMove)
                continue;

            // get the number of tiles
            var score = getScore(game);

            // set the orginal move
            var rootMove = -1;
            if (parentNode.rootMove == -1)
                rootMove = move;
            else
                rootMove = parentNode.rootMove;

            // create a child node
            var node = getNode(
                parentNode.id.toString() + move.toString(),
                nodeLevel,
                game,
                move,
                rootMove,
                score
            );
            
            // recurse to process child nodes
            loadGameTree(node, maxHeight, positions);
            
            // we've reached the end, collect the node
            positions.push(node);
        }
    }

    function getBestMove(positions) {

        // rank the postions based on their score
        positions.sort(function(a, b) { 
            return b.score - a.score; 
        });

        // capture best moves
        var bestMoves = [];
        var maxScore = positions[0].score;
        var i = 0;
        while (i < positions.length && positions[i].score == maxScore) {
            bestMoves.push(positions[i]);
            i++;
        }

        // sort by length of path
        bestMoves.sort(function(a, b) { 
            return a.level - b.level;
        });

        // capture shortest moves
        var shortestMoves = [];
        var minDist = bestMoves[0].level;
        var j = 0;        
        while (j < bestMoves.length && bestMoves[j].level == minDist) {
            shortestMoves.push(bestMoves[j]);
            j++;
        }
     
        // return shortest move
        return shortestMoves[0].rootMove;
    }     

    function getNode(
        nodeId, 
        nodeLevel, 
        gameState, 
        currentMove, 
        rootMove,
        score
    ) {
        return {
            id: nodeId,
            level: nodeLevel,
            game: gameState,
            move: currentMove,
            rootMove: rootMove,
            score: score
        };
    } 

    // get the sum of all tile values
    function getScore(grid) {
        var cells = grid.cells;
        var i = cells.length;
        var j = cells[0].length
        var tiles = [];

        // add all non null cells to tiles[]
        for (i=0; i<cells.length; i++)
            for (j=0; j<cells[0].length; j++)
                if (cells[i][j]) {
                    var value = cells[i][j].value;
                    tiles.push(value * value);
                }

        // add up the values in the array
        var sum = tiles.reduce(function(a, b) { return a + b; });
        
        return sum;
    }
}

