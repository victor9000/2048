// the Ai.step function implements a greedy algorithm which
// attempts to minimize the number of tiles with each turn.
// the algorithm only considers whether or not tiles can merge,
// and moves in that direction if they can.  It offers no 
// look-ahead , and it does not consider the values of any tile.

function Ai() {
 
    this.init = function() {
        // This method is called when AI is first initialized.
    }
     
    this.restart = function() {
        // This method is called when the game is reset.
    }
     
    // this.step is called on every update.
    // Return one of these integers to move tiles on the grid:
    // 0: up, 1: right, 2: down, 3: left

    // Parameter grid contains current state of the game as Tile objects stored in grid.cells.
    // Top left corner is at grid.cells[0][0], top right: grid.cells[3][0], bottom left: grid.cells[0][3], bottom right: grid.cells[3][3].
    // Tile objects have .value property which contains the value of the tile. If top left corner has tile with 2, grid.cells[0][0].value == 2.
    // Array will contain null if there is no tile in the slot (e.g. grid.cells[0][3] == null if bottom left corner doesn't have a tile).

    // Grid has 2 useful helper methods:
    // .copy()    - creates a copy of the grid and returns it.
    // .move(dir) - can be used to determine what is the next state of the grid going to be if moved to that direction.
    //              This changes the state of the grid object, so you should probably copy() the grid before using this.
    //              Naturally the modified state doesn't contain information about new tiles.
    //              Method returns true if you can move to that direction, false otherwise.
    this.step = function(grid) {     
        var origin = grid.copy();
        var minCount = getTileCount(grid.cells);
        var move = -1;

        // see if moves will lead to less tiles
        for (i=0; i<4; i++) {
            if(grid.move(i)) {
                var count = getTileCount(grid.cells);

                // if the move leads to less tiles then take it
                if(count < minCount) {
                    move = i;
                    minCount = count;
                }
                // if there is a tie then choose randomly
                else if(count === minCount) {
                    var rand = Math.random();
                    if (rand < 0.5)
                        move = i;
                }

                grid = origin.copy();
            }
        }

        // no moves will lead to less tiles
        if(move === -1)
            move = getBiasedMove(origin);

        return move;
    }

    function getTileCount(cells) {
        var i = cells.length;
        var j = cells[0].length
        var tiles = [];

        for (i=0; i<cells.length; i++)
            for (j=0; j<cells[0].length; j++) 
                if (cells[i][j])
                    tiles.push(cells[i][j].value);

        return tiles.length;
    }

    function getBiasedMove(grid) {
        nextMove = -1;
        dirs = [0, 1, 3, 2];

        for (i=0; i<4 && nextMove === -1; i++) {
            if(grid.move(dirs[i]))
                nextMove = dirs[i];
        }

        return nextMove;
    }

}

