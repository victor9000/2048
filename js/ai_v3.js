// the Ai.step function implements an algorithm which
// attempts to maximize the sum of the squares of each title
// using a game tree for look aheads.  This means that the
// algorithm will consider a 4 tile to be more valueable than
// two 2 tiles, since 4^2 > 2^2 + 2^2.

function Ai() {

    this.init = function() {
        // This method is called when AI is first initialized.
    }

    this.restart = function() {
        // This method is called when the game is reset.
    }

    // this.step is called on every update.
    // Return one of these integers to move tiles on the grid:
    // 0: up, 1: right, 2: down, 3: left

    // Parameter grid contains current state of the game as Tile objects stored in grid.cells.
    // Top left corner is at grid.cells[0][0], top right: grid.cells[3][0], bottom left: grid.cells[0][3], bottom right: grid.cells[3][3].
    // Tile objects have .value property which contains the value of the tile. If top left corner has tile with 2, grid.cells[0][0].value == 2.
    // Array will contain null if there is no tile in the slot (e.g. grid.cells[0][3] == null if bottom left corner doesn't have a tile).

    // Grid has 2 useful helper methods:
    // .copy()    - creates a copy of the grid and returns it.
    // .move(dir) - can be used to determine what is the next state of the grid going to be if moved to that direction.
    //              This changes the state of the grid object, so you should probably copy() the grid before using this.
    //              Naturally the modified state doesn't contain information about new tiles.
    //              Method returns true if you can move to that direction, false otherwise.
    this.step = function(grid) {
        var id = '';
        var depth = 6;
        var mergeScore = getMergeScore(grid);
        var distanceScore = getDistanceScore(grid);
        var root = getNode(id, -1, grid, -1, -1, mergeScore, distanceScore);
        var positions = [];
        var move = -1;

        // create a game tree of each possible move,
        // and assign a score to each position
        processGameTree(root, depth, positions);

        // get the best move from the scored positions
        if (positions && positions.length > 0)
            move = getBestMove(positions);

        return move;
    }

    function processGameTree(parentNode, maxHeight, positions) {
        var nodeLevel = parentNode.level + 1;
        var parentHasChildMoves = false;

        // don't go further if we've reached the max height
        if (nodeLevel == maxHeight)
            return;

        // get moves
        for (var i=0; i<4; i++) {
            var move = i;
            var game = parentNode.game.copy();
            var canMove = game.move(move);

            // if we can't move then skip to next the move
            if(canMove)
                parentHasChildMoves = true;
            else {
                if (i == 3 && !parentHasChildMoves) {
                    parentNode.mergeScore = Number.MIN_VALUE;
                    parentNode.distScore = Number.MAX_VALUE;
                }
                continue;
            }

            // assign a score to this position which scores
            // merged tiles higher than unmerged tiles.
            // higher is better.
            var mergeScore = getMergeScore(game);

            // assign a score to this position based on
            // the distance between similar tiles.
            // lower is better.
            var distScore = getDistanceScore(game);

            // place a new tile on the game board
            // to simulate proper game play.
            var cpuCell = game.randomAvailableCell();
            var cpuTile = new Tile(cpuCell, 2);
            game.insertTile(cpuTile);

            // set root move.  this is the parent move that
            // got us to our current position.  this is the
            // move we play if we end up in some great position
            // somewhere down the game tree.
            var rootMove = -1;
            if (parentNode.rootMove == -1)
                rootMove = move;
            else
                rootMove = parentNode.rootMove;

            // create a child node
            var node = getNode(
                parentNode.id.toString() + move.toString(),
                nodeLevel,
                game,
                move,
                rootMove,
                mergeScore,
                distScore,
                -1
            );

            // recurse to process child nodes
            processGameTree(node, maxHeight, positions);

            // we've reached the end, collect the node
            positions.push(node);
        }
    }

    function getBestMove(positions) {
        // find the maximum possible merge score.
        // bigger is better.
        var maxMergePos = positions.reduce(function(prev, current) {
            return current.mergeScore > prev.mergeScore ? current : prev;
        });

        // find the maximum possible distance score.
        // smaller is better.
        var maxDistPos = positions.reduce(function(prev, current) {
            return current.distScore > prev.distScore ? current : prev;
        });

        // create a composite score base on mergeScore and distScore.
        // bigger is better.
        var nonZeroMaxDist = Math.max(maxDistPos.distScore, (1 / Number.MAX_VALUE));
        positions.forEach(function(element) {
            element.overallScore =
                ((element.mergeScore / maxMergePos.mergeScore) * 50) +
                (((maxDistPos.distScore - element.distScore) / nonZeroMaxDist) * 50);
        });

        // order the positions by the composite score.
        // bigger is better.
        positions.sort(function (a, b) {
            return b.overallScore - a.overallScore;
        });

        // collect the positions with the largest composite score.
        var maxCompositeScoreMoves = positions.filter(function(element) {
            return element.overallScore == positions[0].overallScore;
        });

        // order the largest composite scores by depth.
        // smaller is better.
        maxCompositeScoreMoves.sort(function (a, b) {
            return a.level - b.level;
        });

        // collect the positions with the smallest depth.
        var minDepthMoves = maxCompositeScoreMoves.filter(function(element) {
            return element.level == maxCompositeScoreMoves[0].level;
        });

        // apply a directional bias of: 0, 1, 3, 2
        minDepthMoves.sort(function(a, b) {
            var moveA = a.rootMove;
            var moveB = b.rootMove;

            if (moveA == 2)
                moveA = 4;
            if (moveB == 2)
                moveB = 4;

            return moveA - moveB;
        });

        return minDepthMoves[0].rootMove;
    }

    function getNode(
        nodeId,
        nodeLevel,
        gameState,
        currentMove,
        rootMove,
        mergeScore,
        distScore,
        overallScore
    ) {
        return {
            id: nodeId,
            level: nodeLevel,
            game: gameState,
            move: currentMove,
            rootMove: rootMove,
            mergeScore: mergeScore,
            distScore: distScore,
            overallScore: overallScore
        };
    }

    function getMergeScore(grid) {
        var cells = grid.cells;
        var tiles = [];

        // add all non null cells to tiles[]
        for (var i=0; i<cells.length; i++)
            for (var j=0; j<cells[0].length; j++)
                if (cells[i][j]) {
                    var value = cells[i][j].value;

                    // score merged tiles higher than individual tiles
                    tiles.push(value * value);
                }

        // add up the values in the array
        var sum = tiles.reduce(function(a, b) { return a + b; });

        return sum;
    }

    function getDistanceScore(grid) {
        var groupedTiles = {};

        // group all tiles by their value
        for (var i=0; i<grid.size; i++) {
            for (var j=0; j<grid.size; j++) {
                var cell = grid.cells[i][j];
                if(!cell)
                    continue;

                if(!(cell.value in groupedTiles))
                    groupedTiles[cell.value] = [ cell ];
                else
                    groupedTiles[cell.value].push(cell);
            }
        }

        var globalDist = 0;
        var avgGlobalDist = -1;
        var keys = Object.keys(groupedTiles);

        for (var i=0; i<keys.length; i++) {
            var key = keys[i];
            var fromPoint = groupedTiles[key][0];
            var localDist = 0;

            for (var j = 1; j < groupedTiles[key].length; j++) {
                var toPoint = groupedTiles[key][j];
                localDist += getDistance(fromPoint, toPoint);
            };

            globalDist += localDist;
        }

        avgGlobalDist = globalDist / keys.length;

        return avgGlobalDist;
    }

    function getDistance(p1, p2) {
        return Math.sqrt(Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2));
    }
}

